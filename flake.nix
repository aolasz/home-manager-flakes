{
  description = "A standalone Home Manager configuration";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    home-manager = {
      url = "github:nix-community/home-manager/release-22.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... } @inputs:

    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      username = "jdoe";
      stateVersion = "22.05";

    in {
      homeConfigurations.${username} = home-manager.lib.homeManagerConfiguration {
        inherit pkgs stateVersion system username ;
        extraSpecialArgs = { inherit inputs; };
        homeDirectory = "/home/${username}";
        configuration = { ... }: {
          imports = [
            ./home.nix
          ];
        };
          # Specify your home configuration modules here, for example,
          # the path to your home.nix.

          # Optionally use extraSpecialArgs
          # to pass through arguments to home.nix
      };
    };
}
