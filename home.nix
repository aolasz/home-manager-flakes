{
  programs.home-manager.enable = true;

  programs.bash = {
    enable = true;
    bashrcExtra = ''
      . ~/.bashrc.old
    '';
  };

  # home.packages = with pkgs; [
  #   tmux
  # ];
}
